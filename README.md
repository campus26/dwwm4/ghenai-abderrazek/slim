# Projet de Recherche de Factures et Clients avec Slim

Ce projet utilise le framework Slim pour créer une API permettant de rechercher des factures et des clients sans nécessiter de connexion.

## Prérequis

- PHP >= version
- Composer installé localement
- Serveur web (Apache, Nginx, etc.)

## Installation

1. Cloner le dépôt 
2. Se déplacer dans le répertoire du projet
3. Installer les dépendances PHP via Composer : `composer install`
4. Les librairies suivantes ont été installées pour démarrer: le projet slim/slim slim/psr7

## Utilisation

1. Lancez votre serveur web local php -S localhost:9000.
2. Accédez à votre application via votre navigateur, en utilisant les routes définies dans votre application Slim.

## Fonctionnalités

L'API offre les fonctionnalités suivantes :

- Recherche de factures par numéro de facture, nom du client, date, etc.
- Recherche de clients par nom, adresse, etc.

## Déploiement

Pour déployer ce projet en production, suivez les étapes suivantes :

1. Configurez un serveur web pour servir l'application Slim.
2. Assurez-vous que le serveur possède les bonnes permissions sur les répertoires de cache et de logs.
3. Configurez les variables d'environnement sur le serveur en ajoutant un fichier `.env` contenant les informations sensibles.



