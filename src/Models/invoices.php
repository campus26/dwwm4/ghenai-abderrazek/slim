<?php

namespace Cp26\Dwwm4\Models;
use Cp26\Dwwm4\Models\DataBase;


class invoices extends DataBase
{


    public function __construct()
    {
    }

    public function getInvoices()
    {

        $invoiceId = $_GET['invoiceId'];

        $requete = $this->getPDO()->prepare("SELECT * FROM invoices WHERE id LIKE :invoiceId");

        $keyWord = $invoiceId;
        $requete->bindParam(':invoiceId', $keyWord);
        $requete->execute();
        $rep = $requete->fetchAll(\PDO::FETCH_ASSOC);
        return $rep;
    }
}
