<?php

namespace Cp26\Dwwm4\Models;


use Cp26\Dwwm4\Models\DataBase;


class customers extends DataBase
{
    public function __construct()
        {
        }

    public function getCustomers()
    {
        

        $name = $_GET['name'];
        // $surname = $_GET['surname'];



        $requete = $this->getPDO()->prepare("SELECT * FROM customers WHERE name LIKE :name  OR surname LIKE :surname LIMIT 50");
        $keyWord = "%" . $name . "%";
        $requete->bindParam(':name', $keyWord);
        $requete->bindParam(':surname', $keyWord);
        $requete->execute();
        $rep = $requete->fetchAll(\PDO::FETCH_ASSOC);
        return $rep;
    }
}
