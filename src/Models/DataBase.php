<?php

namespace Cp26\Dwwm4\Models;

use Dotenv\Dotenv;
use PDO;
use PDOException;

class DataBase
{
    // Instance de la classe PDO
    private static $pdo;

    // Informations de connexion à la base de données
    private $host = '';
    private $dbname = '';
    private $user = '';
    private $password = '';
    private $charset = '';

    // Constructeur privé pour empêcher la création d'instances directes
    private function __construct() {}

    // Méthode statique pour obtenir l'instance unique de la classe PDO
    public static function getPDO()
    {
        if (!self::$pdo) {
            // Charge les variables d'environnement à partir du fichier .env
            $dotenv = Dotenv::createImmutable(__DIR__ . '/../../');
            $dotenv->load();

            $host = getenv('DB_HOST');
            $dbname = getenv('DB_NAME');
            $user = getenv('DB_USER');
            $password = getenv('DB_PASSWORD');
            $charset = getenv('DB_CHARSET');

            // Chaîne de connexion PDO (Data Source Name) qui contient les informations de la base de données
            $dsn = "mysql:host={$host};dbname={$dbname};charset={$charset}";

            // Options de configuration PDO, déterminent le comportement de PDO pendant la durée de vie de cette instance.
            $options = [
                PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
                PDO::ATTR_EMULATE_PREPARES   => false,
            ];

            try {
                // Tentative de création d'une instance PDO
                self::$pdo = new PDO($dsn, $user, $password, $options);
            } catch (PDOException $e) {
                // En cas d'échec, capture de l'exception et relance avec un message personnalisé
                throw new PDOException($e->getMessage(), (int)$e->getCode());
            }
        }

        // Retourne l'instance unique de la classe PDO
        return self::$pdo;
    }
}
