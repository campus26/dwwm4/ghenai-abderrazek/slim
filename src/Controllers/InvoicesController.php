<?php

namespace Cp26\Dwwm4\Controllers;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Cp26\Dwwm4\Models\invoices;


class InvoicesController extends TwigController
{
    public function getAll(Request $request, Response $response)
    {
        $invoicesobj = new invoices(); //instance de mon objet invoices qui se trouve dans invoices.php
        $invoicesobj->getInvoices(); // appeler la fonction getTop... dans l'instance $invoicesobj
        $data = $invoicesobj->getInvoices(); // mettre le tout dans une variable data
        $template = $this->template('invoices.twig');

        $response->getBody()->write($template->render([
            'pageTitle' => 'Titre',
            'content' => 'Contenu',
            'invoicesData' => $data,


        ]));

        return $response;
    }

    public function get(Request $request, Response $response)
    {

        $template = $this->template('invoice.twig');

        $response->getBody()->write($template->render([
            'pageTitle' => 'Titre',
            'content' => 'Contenu',
        ]));

        return $response;
    }
}
