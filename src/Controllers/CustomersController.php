<?php

namespace Cp26\Dwwm4\Controllers;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Cp26\Dwwm4\Models\Customers;
use Cp26\Dwwm4\Models\Customer;

class CustomersController extends TwigController
{
    public function getAll(Request $request, Response $response)
    {
        $customersobj = new customers(); //instance de mon objet customers qui se trouve dans customers.php
        $customersobj->getCustomers(); // appeler la fonction getCustomers.. dans l'instance $customersobj
        $data = $customersobj->getCustomers(); // mettre le tout dans une variable data
        $searchDone = !empty($_GET['name']);
        $template = $this->template('customers.twig');

        $response->getBody()->write($template->render([
            'pageTitle' => 'Titre',
            'content' => 'Contenu',
            'customersData' => $data,
            'searchDone' => $searchDone
        ]));

        return $response;
    }

    public function get(Request $request, Response $response)
    {

        $customerobj = new customer(); //instance de mon objet  qui se trouve dans customer.php
        //$customerobj->getCustomer(); // appeler la fonction getcsutom... dans l'instance $customerobj
        $data = $customerobj->getCustomer();

        $searchDone = !empty($_GET['name']);
        $template = $this->template('customer.twig');

        $response->getBody()->write($template->render([
            'pageTitle' => 'Titre',
            'content' => 'Contenu',
            'customerData' => $data,
            'searchDone' => $searchDone

        ]));

        return $response;
    }
}
